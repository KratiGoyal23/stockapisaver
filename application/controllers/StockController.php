<?php

class StockController extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->database(); // Initialize the database
    }

    public function fetchStockData() {
        $stock_list = [
            'ASIANPAINT.NS', 'BRITANNIA.NS', 'CIPLA.NS', 'EICHERMOT.NS', 'NESTLEIND.NS',
            'GRASIM.NS', 'HEROMOTOCO.NS', 'HINDALCO.NS', 'HINDUNILVR.NS', 'ITC.NS',
            'LT.NS', 'M&M.NS', 'RELIANCE.NS', 'TATACONSUM.NS', 'TATAMOTORS.NS',
            'TATASTEEL.NS', 'WIPRO.NS', 'APOLLOHOSP.NS', 'DRREDDY.NS', 'TITAN.NS',
            'SBIN.NS', 'BPCL.NS', 'KOTAKBANK.NS', 'UPL.NS', 'INFY.NS',
            'BAJFINANCE.NS', 'ADANIENT.NS', 'SUNPHARMA.NS', 'JSWSTEEL.NS', 'HDFCBANK.NS',
            'TCS.NS', 'ICICIBANK.NS', 'POWERGRID.NS', 'MARUTI.NS', 'INDUSINDBK.NS',
            'AXISBANK.NS', 'HCLTECH.NS', 'ONGC.NS', 'NTPC.NS', 'COALINDIA.NS',
            'BHARTIARTL.NS', 'TECHM.NS' , 'DIVISLAB.NS',
            'ADANIPORTS.NS', 'HDFCLIFE.NS', 'SBILIFE.NS', 'ULTRACEMCO.NS', 'BAJAJ-AUTO.NS',
            'BAJAJFINSV.NS',
        ];
        date_default_timezone_set('Asia/Kolkata');
        foreach ($stock_list as $sname) {
            
            $api_url = "https://query1.finance.yahoo.com/v8/finance/chart/${sname}?range=1d&interval=2m";
            $ch = curl_init();
            //  date_default_timezone_set('Asia/Kolkata');
            curl_setopt($ch, CURLOPT_URL, $api_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);

            if ($response !== false) {
                // Process and store data here as before
                $data = json_decode($response, true);
                $regularMarketPrice = $data['chart']['result'][0]['meta']['regularMarketPrice'];
                // Insert data into the database
                $insert_data = array(
                    'stock_name' => $sname,
                    'price' => $regularMarketPrice,
                );
                $this->db->insert('stocks_data', $insert_data);
            } else {
                echo 'cURL Error for ' . $sname . ': ' . curl_error($ch);
            }

            curl_close($ch);
        }
        
        echo 'Data fetched and stored successfully!';
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Stock Data</title>
</head>
<body>
    <h1>Stock Data</h1>
    <p>Data fetched and stored successfully!</p>

    <script>
        // Reload the page every 2 minutes
        setTimeout(function() {
            location.reload();
        }, 120000); // 120,000 milliseconds = 2 minutes
    </script>
</body>
</html>



