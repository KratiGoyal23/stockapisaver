<!DOCTYPE html>
<html>
<head>
    <title>Stock Price Chart</title>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
</head>
<body>
    <canvas id="stockChart"></canvas>

    <script>
        // Assuming you have received the stock data as $data from the controller
        var labels = <?php echo json_encode($data['chart']['result'][0]['timestamp']); ?>;
        var prices = <?php echo json_encode($data['chart']['result'][0]['indicators']['quote'][0]['close']); ?>;
        
        var formattedLabels = labels.map(timestamp => new Date(timestamp * 1000).toLocaleTimeString());

        var ctx = document.getElementById('stockChart').getContext('2d');
        var stockChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: formattedLabels,
                datasets: [{
                    label: 'Stock Price',
                    data: prices,
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 1,
                    pointRadius: 0
                }]
            },
            options: {
                scales: {
                    x: {
                        type: 'time',
                        time: {
                            unit: 'minute',
                            displayFormats: {
                                minute: 'HH:mm'
                            }
                        }
                    }
                }
            }
        });
    </script>
</body>
</html>
